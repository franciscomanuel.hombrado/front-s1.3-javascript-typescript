function init() {
    ( < HTMLInputElement > document.getElementById("result")).value = ""
}

function resetear() {
    ( < HTMLInputElement > document.getElementById("result")).value = ""
}

function clickBoton(id: string) {
    document.getElementById(id).onclick = function () {
        ( < HTMLInputElement > document.getElementById("result")).value +=
            ( < HTMLInputElement > document.getElementById(id)).value

    }
}

function realizaOperacion() {
        let operacion = ( < HTMLInputElement > document.getElementById("result")).value

        let operadores: string[] = buscaOperadores(operacion)
        let signos: string[] = buscaSignos(operacion)

        let resultado_final = Number(operadores[0])

        for(let i = 0; i < signos.length; i++){
            resultado_final = operacionSimple(signos[i],
                resultado_final, Number(operadores[i+1]))
        }
        
        (<HTMLInputElement>document.getElementById("result")).value = String(resultado_final)
}

function buscaOperadores(ope: string): string[] {
    let operadores = []
    let operador = ""

    let primero = 0

    ope.split("").forEach(element => 
        {
            if(esSigno(element) && primero == 0){
                if(element == '-'){
                    operador += element
                }                
            }

            if(esNumero(element) || esPunto(element)){
                operador += element
            }

            if(esSigno(element) && primero > 0){
                operadores.push(operador)
                operador = ""
            }

            primero++
        }
    )
    operadores.push(operador)

    return operadores
}

function buscaSignos(ope: string): string[] {
    let signos = []

    let primero = 0

    ope.split("").forEach(element => 
        {
            if(esSigno(element) && primero > 0){
                signos.push(element)
            }

            primero++
        }
    )

    return signos
}

function operacionSimple(signo: string, ope1: number, ope2: number){
    switch(signo){
        case '+':
            return ope1 + ope2
        case '-':
            return ope1 - ope2      
        case '*':
            return ope1 * ope2     
        case '/':
            return ope1 / ope2      
        case '^':
            return ope1 ** ope2
    }
}

function esNumero(element: string): boolean {
    if (element == '0' ||
        element == '1' ||
        element == '2' ||
        element == '3' ||
        element == '4' ||
        element == '5' ||
        element == '6' ||
        element == '7' ||
        element == '8' ||
        element == '9') {
        return true;
    } 
    return false;
}

function esPunto(element: string): boolean {
    if (element == '.') {
        return true;
    }
    return false;
}

function esSigno(element: string): boolean {
    if (element == '+' ||
        element == '-' ||
        element == '*' ||
        element == '/' ||
        element == '^') {
        return true;
    } 
    return false;
}

window.onload = function () {
    init()

    document.getElementById('C').onclick = resetear

    clickBoton('0')
    clickBoton('1')
    clickBoton('2')
    clickBoton('3')
    clickBoton('4')
    clickBoton('5')
    clickBoton('6')
    clickBoton('7')
    clickBoton('8')
    clickBoton('9')

    clickBoton('punto')
    clickBoton('exp')
    clickBoton('entre')
    clickBoton('por')
    clickBoton('menos')
    clickBoton('mas')

    document.getElementById('igual').onclick = realizaOperacion
}