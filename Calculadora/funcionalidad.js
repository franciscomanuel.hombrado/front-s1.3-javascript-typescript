function init() {
    document.getElementById("result").value = "";
}
function resetear() {
    document.getElementById("result").value = "";
}
function clickBoton(id) {
    document.getElementById(id).onclick = function () {
        document.getElementById("result").value +=
            document.getElementById(id).value;
    };
}
function realizaOperacion() {
    var operacion = document.getElementById("result").value;
    var operadores = buscaOperadores(operacion);
    var signos = buscaSignos(operacion);
    var resultado_final = Number(operadores[0]);
    for (var i = 0; i < signos.length; i++) {
        resultado_final = operacionSimple(signos[i], resultado_final, Number(operadores[i + 1]));
    }
    document.getElementById("result").value = String(resultado_final);
}
function buscaOperadores(ope) {
    var operadores = [];
    var operador = "";
    var primero = 0;
    ope.split("").forEach(function (element) {
        if (esSigno(element) && primero == 0) {
            if (element == '-') {
                operador += element;
            }
        }
        if (esNumero(element) || esPunto(element)) {
            operador += element;
        }
        if (esSigno(element) && primero > 0) {
            operadores.push(operador);
            operador = "";
        }
        primero++;
    });
    operadores.push(operador);
    return operadores;
}
function buscaSignos(ope) {
    var signos = [];
    var primero = 0;
    ope.split("").forEach(function (element) {
        if (esSigno(element) && primero > 0) {
            signos.push(element);
        }
        primero++;
    });
    return signos;
}
function operacionSimple(signo, ope1, ope2) {
    switch (signo) {
        case '+':
            return ope1 + ope2;
        case '-':
            return ope1 - ope2;
        case '*':
            return ope1 * ope2;
        case '/':
            return ope1 / ope2;
        case '^':
            return Math.pow(ope1, ope2);
    }
}
function esNumero(element) {
    if (element == '0' ||
        element == '1' ||
        element == '2' ||
        element == '3' ||
        element == '4' ||
        element == '5' ||
        element == '6' ||
        element == '7' ||
        element == '8' ||
        element == '9') {
        return true;
    }
    return false;
}
function esPunto(element) {
    if (element == '.') {
        return true;
    }
    return false;
}
function esSigno(element) {
    if (element == '+' ||
        element == '-' ||
        element == '*' ||
        element == '/' ||
        element == '^') {
        return true;
    }
    return false;
}
window.onload = function () {
    init();
    document.getElementById('C').onclick = resetear;
    clickBoton('0');
    clickBoton('1');
    clickBoton('2');
    clickBoton('3');
    clickBoton('4');
    clickBoton('5');
    clickBoton('6');
    clickBoton('7');
    clickBoton('8');
    clickBoton('9');
    clickBoton('punto');
    clickBoton('exp');
    clickBoton('entre');
    clickBoton('por');
    clickBoton('menos');
    clickBoton('mas');
    document.getElementById('igual').onclick = realizaOperacion;
};
