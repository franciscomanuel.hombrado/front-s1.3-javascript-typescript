const URL = 'http://localhost:8080/persona';

//Devolver todas las personas y mostrar solo los datos de la primera
function mostrarDatosPrimeraPersona(){
    fetch(URL)
    .then(response => response.json()
    .then(data =>{
        if(data.length > 0){
            let primero = 0;
            let primerElemento = data[primero];

            document.getElementById('usuario').value = primerElemento.usuario;
            document.getElementById('apellido').value = primerElemento.surname;
            document.getElementById('contrasena').value = primerElemento.password;
            document.getElementById('nombre').value = primerElemento.name;
            document.getElementById('email_compania').value = primerElemento.company_email;
            document.getElementById('email_personal').value = primerElemento.personal_email;
            document.getElementById('ciudad').value = primerElemento.city;
            document.getElementById('url_imagen').value = primerElemento.imagen_url;
            document.getElementById('fecha_creacion').value = primerElemento.created_date;
            if(primerElemento.active){
                document.getElementById('activado').value = true;
            }else{
                document.getElementById('activado').value = false;
            }
            document.getElementById('fecha_finalizacion').value = primerElemento.termination_date;
            document.getElementById('id_persona').value = primerElemento.id_persona;
        }
    }))
}

//Limpiar campos
function limpiarCampos(event){
    document.getElementById('usuario').value = "";
    document.getElementById('apellido').value = "";
    document.getElementById('contrasena').value = "";
    document.getElementById('nombre').value = "";
    document.getElementById('email_compania').value = "";
    document.getElementById('email_personal').value = "";
    document.getElementById('ciudad').value = "";
    document.getElementById('url_imagen').value = ""
    document.getElementById('fecha_creacion').value = "";
    document.getElementById('activado').value = false;
    document.getElementById('fecha_finalizacion').value = "";
    document.getElementById('id_persona').value = "";
}

//Modificar datos
function modificarPersona(event){
    event.preventDefault();

    const data = {
        usuario : document.getElementById('usuario').value,
        password : document.getElementById('contrasena').value,
        name : document.getElementById('nombre').value,
        surname : document.getElementById('apellido').value,
        company_email : document.getElementById('email_compania').value,
        personal_email : document.getElementById('email_personal').value,
        city : document.getElementById('ciudad').value,
        active : document.getElementById('activado').value,
        created_date : document.getElementById('fecha_creacion').value,
        imagen_url : document.getElementById('url_imagen').value,
        termination_date : document.getElementById('fecha_finalizacion').value
    }

    let URL_MOD_PERSONA = URL + '/' + document.getElementById('id_persona').value;

    fetch(URL_MOD_PERSONA, {
        method: 'PUT',
        headers:{ 'Content-Type':'application/json'},
        body : JSON.stringify(data)
    })
}

window.onload = function(){
    mostrarDatosPrimeraPersona();

    document.getElementById('enviar').onclick = modificarPersona;
    document.getElementById('limpiar').onclick = limpiarCampos;
}